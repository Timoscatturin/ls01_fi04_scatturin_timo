﻿import java.util.Scanner;

 

class Fahrkartenautomat {
    public static void main(String[] args) {
    	while(true){
        Scanner tastatur = new Scanner(System.in);

 

        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag;
        int anzahlTicket;
        
 

        zuZahlenderBetrag = farkartenBestellungErfassen();

 

        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

 

        fahrkartenAusgeben();

 

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        
    	}
    }

 
    public static double farkartenBestellungErfassen() {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	int[] auswahlnummer= {0,1,2,3,4,5,6,7,8,9,10};
    	String[] bezeichnung={"0","Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC","Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
    	double[] preisInEuro= {0,2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};    	
        double zuZahlenderBetrag = 0 ;
        
        System.out.printf("%-20s %-40s %10s\n","Auswahlnummer","Bezeichnung","Preis in Euro");
        System.out.println("________________________________________________________________________________________");
        
        int i;
        for ( i=1; i<10; i++) 
        {
        	System.out.printf("%-20s %-40s %10s\n",auswahlnummer[i],  bezeichnung[i],  preisInEuro[i] + "0€" );
        }
        
        System.out.println();
        System.out.println("Bitte wählen sie ihre Fahrkarte aus!");
        int auswahl = tastatur.nextInt();
        
		System.out.println("Sie haben sich für: " + bezeichnung[auswahl] + " entschieden");
		System.out.println("Die Karte kostet: " + preisInEuro[auswahl] + "0€");
		
		
        
        
        int anzahlTicket = 0;
        System.out.print("Anzahl der Tickets: ");
        while (anzahlTicket == 0) 
        {
         anzahlTicket = tastatur.nextInt();
        if( anzahlTicket >=10 || anzahlTicket <1) {
        	System.out.println("Da ist etwas schiefgelaufen. Geben Sie eine Anzahl zwischen 1 und 10 ein.");
       	 	anzahlTicket = 0;}
       	 else {
       		 
       		 anzahlTicket = anzahlTicket;
       	 
        }
        }
        
        

        System.out.print("Zu zahlender Betrag (EURO): ");
        

 

        zuZahlenderBetrag = preisInEuro[auswahl] * anzahlTicket;
        return zuZahlenderBetrag;
        
    }

 
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);

 

        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            double ausstehendeBetrage = zuZahlenderBetrag;
            ausstehendeBetrage = ausstehendeBetrage - eingezahlterGesamtbetrag;
            System.out.printf("Noch zu zahlen: %.2f €\n", ausstehendeBetrage);
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }

 

        return eingezahlterGesamtbetrag;
    }

 
    public static void fahrkartenAusgeben() {
        Scanner tastatur = new Scanner(System.in);

 

        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

   
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);

 

        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rückgabebetrag > 0.0) {
            System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

 

            while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
            while (rückgabebetrag >= 0.02)// 2 CENT-Münzen
            {
                System.out.println("2 CENT");
                rückgabebetrag -= 0.02;
            }
            while (rückgabebetrag >= 0.01)// 1 CENT-Münzen
            {
                System.out.println("1 CENT");
                rückgabebetrag -= 0.01;
            }
        }

 

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir wünschen Ihnen eine gute Fahrt.\n");

 

        
    }
    
    
    
    
}